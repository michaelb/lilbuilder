lilbuilder
==========

Simple build automation script. It is useful when setting up quick and dirty CI
or build servers on machines where pinging a git repo for changes is easier
than listening for push or manual triggers.

Basically, it does the following:

1. Pull new code

2. Check if its already built this commit hash (or version-string)

3. If it hasn't, then run a build command, and move an output directory to a
predefined location

4. Exit; or if specified, loop back to #1

usage
-----

```
usage: lilbuilder.py [-h] [-c COMMAND] [-d DESTINATION] [-w WORKSPACE]
                     [-a ARTIFACT] [-p PULL] [-b BRANCH] [-q] [-n NAMECMD]
                     [-P] [-W] [-S SLEEP]

optional arguments:
  -h, --help            show this help message and exit
  -w WORKSPACE, --workspace WORKSPACE
                        working directory of checked out repo
  -a ARTIFACT, --artifact ARTIFACT
                        relative to git repo, location to get build artifacts
  -p PULL, --pull PULL  pull command used to update repo
  -b BRANCH, --branch BRANCH
                        branch to checkout
  -q, --quiet           silence lilbuilder output
  -n NAMECMD, --namecmd NAMECMD
                        command to run to get release identifier (uses git log
                        by default)
  -P, --packagejson     shortcut to enabling usage of package.json to get
                        version number
  -W, --watch           enable watch mode: instead of just running once,
                        repeat infinitely
  -S SLEEP, --sleep SLEEP
                        if watch is enabled, specify delay, in seconds,
                        between checks

required:
  -c COMMAND, --command COMMAND
                        command that triggers actual build, e.g. 'make'
  -d DESTINATION, --destination DESTINATION
                        target directory to move completed artifacts

```

