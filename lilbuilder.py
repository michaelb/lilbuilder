#!/usr/bin/env python3
'''
A small tool for tiny, hacked-together build systems.

Given a Git repository and a destination directory, it will check it out,
see if it has not yet built this version of the code, and if it hasn't
it will run a command and move the output directory.

The 'version' by default is the short-hash, but can be determined by a
custom command (e.g., for use with git tags, or parsing something like
setup.py or package.json).

It was written for minimal set-up to be run on cron, so that binaries
can be produced on machines without needing the git repository the
ability to push to the build server, or humans to manually kick off
builds.

Tested with Python3, probably runnable on Python2 with modification.

Example test usage, with a dummy build command that creates a single file:
    ./lilbuilder.py --command='mkdir dist && echo 'built' > dist/arty' --destination='/tmp/test_dest/'
'''

__author__ = "michaelb"
__license__ = "MIT"
__version__ = "0.1.0"
__email__ = "michaelpb@gmail.com"

import argparse
import os
import time
import shutil
import subprocess
import json

FILENAME_PREFIX = 'build_'

def scan_dir(dir_path):
    releases = {}
    for basename in os.listdir(dir_path):
        path = os.path.join(dir_path, basename)
        if os.path.isfile(path):
            continue
        if not basename.startswith(FILENAME_PREFIX):
            continue

        # Otherwise, might be relevant
        release_name = basename[len(FILENAME_PREFIX):]
        releases[release_name] = path
    return releases

def log(args, *text_args):
    if not args.quiet:
        print('lilbuilder.py: %s' % ' '.join(text_args))

def get_release_name(args):
    '''
    Return the name of the most recent release, based on given CLI args.
    '''
    if args.packagejson:
        data = json.load(open('package.json'))
        release_name = data.get('version', 'unknown')
    else:
        release_name = subprocess.check_output(args.namecmd, shell=True)
        release_name = release_name.decode('utf-8')
    return release_name.strip()

def build(args):
    '''
    Conditionally build and copy based on args and repo state
    '''
    # Change to relevant directory
    os.chdir(str(args.workspace))

    # Pull to update repo
    pull_cmd = args.pull.replace('%branch%', args.branch)
    subprocess.check_call(pull_cmd, shell=True)

    # Check if already built
    release_name = get_release_name(args)

    mkdir_p(str(args.destination))
    existing = scan_dir(str(args.destination))

    if release_name in existing:
        log(args, "%s already exists, exiting" % release_name)
        return
    log(args, "%s does not yet exist, building" % release_name)

    # Perform build
    subprocess.check_call(args.command, shell=True)

    dest = os.path.join(str(args.destination), FILENAME_PREFIX + release_name)
    artifact = os.path.join(str(args.workspace), str(args.artifact))
    log(args, "Build finished, moving artifacts from %s to %s" % (artifact, dest))
    shutil.move(artifact, dest)

def _parse_args():
    p = argparse.ArgumentParser()
    required = p.add_argument_group('required')
    required.add_argument("-c", "--command", type=str,
        help="command that triggers actual build, e.g. 'make'")
    required.add_argument("-d", "--destination", type=str,
        help="target directory to move completed artifacts")

    p.add_argument("-w", "--workspace", type=str,
        help="working directory of checked out repo",
        default=".")
    p.add_argument("-a", "--artifact", type=str,
        help="relative to git repo, location to get build artifacts",
        default="dist")
    p.add_argument("-p", "--pull", type=str,
        help="pull command used to update repo",
        default="git fetch --all && git reset --hard origin/%branch%")
    p.add_argument("-b", "--branch", type=str,
        help="branch to checkout",
        default="master")
    #p.add_argument("-r", "--repo", help="url to git repository")
    p.add_argument("-q", "--quiet", action="store_true",
        help="silence lilbuilder output")
    p.add_argument("-n", "--namecmd", type=str,
        help="command to run to get release identifier (uses git log by default)",
        default="git log --pretty=format:'%h' -n 1")
    p.add_argument("-P", "--packagejson", action="store_true",
        help="shortcut to enabling usage of package.json to get version number")
    p.add_argument("-W", "--watch", action="store_true",
        help="enable watch mode: instead of just running once, repeat infinitely")
    p.add_argument("-S", "--sleep", type=int,
        help="if watch is enabled, specify delay, in seconds, between checks",
        default=20)
    args = p.parse_args()

    # required:
    if not all([args.command, args.destination]):
        p.parse_args(['-h'])
    return args

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError:
        pass

def watch_loop(args):
    while True:
        build(args)
        log(args, 'Waiting for %i seconds before checking again...' % args.sleep)
        time.sleep(args.sleep)

def main(args):
    if args.watch:
        watch_loop(args)
    else:
        build(args)

if __name__ == '__main__':
    main(_parse_args())
